import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// Import components
import Navbar from './components/Navbar/Navbar';
import Home from './pages/home';
import Shop from './pages/shop';
import Marketplace from './pages/marketplace';
import About from './pages/about';
import Contact from './pages/contact';
import User from './pages/user';

export default function Router() {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/shop' component={Shop} />
        <Route path='/marketplace' component={Marketplace} />
        <Route path='/about' component={About} />
        <Route path='/contact' component={Contact} />
        <Route path='/user' component={User} />
      </Switch>
    </BrowserRouter>
  );
}
