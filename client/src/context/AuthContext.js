import axios from 'axios';
import React from 'react';

const AuthContext = React.createContext();

function AuthContextProvider(props) {
  const [loggedIn, setLoggedIn] = React.useState(undefined);
  // Send cookie to server to see if you are logged in
  async function getLoggedIn() {
    const loggedInRes = await axios.get('auth/loggedIn');
    setLoggedIn(loggedInRes.data);
  }
  // Make loggedIn value true or false so childe components can use it
  React.useEffect(() => {
    getLoggedIn();
  }, []);
  return (
    <AuthContext.Provider value={{ loggedIn, getLoggedIn }}>
      {props.children}
    </AuthContext.Provider>
  );
}

export default AuthContext;
export { AuthContextProvider };
