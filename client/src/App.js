// Import packages
import React, { useState } from 'react';
import axios from 'axios';

// Import components
import Router from './Router';
import { AuthContextProvider } from './context/AuthContext';
// Import styling
import './App.css';

axios.defaults.withCredentials = true;

function App() {
  return (
    <AuthContextProvider>
      <Router />
    </AuthContextProvider>
  );
}

export default App;
