import React from 'react';
import ReactDOM from 'react-dom';
// Import commponent
import App from './App';
// Import styling
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
