import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { RingLoader } from 'react-spinners';
import orderBy from 'lodash/orderBy';

import Buy from '../components/markt_buy';

// Import styling
import '../components/css/markt.css';

// import UI
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import { Select } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

function Marketplace() {
  const [marktInventory, setmarktInventory] = useState(undefined);
  const [ready, setReady] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(6);
  const [columnToSort, setColunToSort] = useState('');
  const [sortDirection, setSortDirection] = useState('desc');
  const [imgUrl, setImgUrl] = useState(undefined);
  const [imgIndex, setImgIndex] = useState(undefined);
  const [query, setQuery] = useState('');
  const [columnToQuery, setColumnToQuery] = useState('name');

  const lowerCaseQuery = query.toLowerCase();

  const inverDirectTion = {
    asc: 'desc',
    desc: 'asc',
  };

  useEffect(async () => {
    const response = await axios.get('markt/inventory');
    console.log(response.data);
    setmarktInventory(response.data);
    setReady(true);
  }, []);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSort = (columnName) => {
    setColunToSort(columnName);
    setSortDirection(
      columnToSort === columnName ? inverDirectTion[sortDirection] : 'desc'
    );
  };

  const mouseActive = (imgUrl, index) => {
    setImgUrl(imgUrl);
    setImgIndex(index);
  };

  const mouseDeactivate = () => {
    setImgUrl(undefined);
    setImgIndex(undefined);
  };

  const handleSelectChange = (event) => {
    setColumnToQuery(event.target.value.toLowerCase());
  };

  return (
    <div>
      {ready ? (
        <div className='table-wrapper'>
          <TableContainer>
            <TextField
              name='name'
              value={query}
              onChange={(e) => setQuery(e.target.value)}
            />
            <Select
              labelId='demo-simple-select-label'
              id='demo-simple-select'
              value={columnToQuery}
              onChange={handleSelectChange}
            >
              <MenuItem value='name'>name</MenuItem>
              <MenuItem value='rarity'>rarity</MenuItem>
              <MenuItem value='set'>set</MenuItem>
              <MenuItem value='quantity'>quantity</MenuItem>
            </Select>
            <Table aria-label='simple table'>
              <TableHead>
                <TableRow>
                  <TableCell onClick={() => handleSort('name')}>
                    Name
                    {columnToSort === 'name' ? (
                      sortDirection === 'asc' ? (
                        <ArrowDropUpIcon />
                      ) : (
                        <ArrowDropDownIcon />
                      )
                    ) : null}
                  </TableCell>
                  <TableCell align='center'>Color</TableCell>
                  <TableCell
                    align='center'
                    onClick={() => handleSort('rarity')}
                  >
                    Rarity
                    {columnToSort === 'rarity' ? (
                      sortDirection === 'asc' ? (
                        <ArrowDropUpIcon />
                      ) : (
                        <ArrowDropDownIcon />
                      )
                    ) : null}
                  </TableCell>
                  <TableCell align='center' onClick={() => handleSort('set')}>
                    Set
                    {columnToSort === 'set' ? (
                      sortDirection === 'asc' ? (
                        <ArrowDropUpIcon />
                      ) : (
                        <ArrowDropDownIcon />
                      )
                    ) : null}
                  </TableCell>
                  <TableCell
                    align='center'
                    onClick={() => handleSort('sellQuantity')}
                  >
                    Quantity
                    {columnToSort === 'sellQuantity' ? (
                      sortDirection === 'asc' ? (
                        <ArrowDropUpIcon />
                      ) : (
                        <ArrowDropDownIcon />
                      )
                    ) : null}
                  </TableCell>
                  <TableCell
                    align='center'
                    onClick={() => handleSort('sellPrice')}
                  >
                    Price
                    {columnToSort === 'sellPrice' ? (
                      sortDirection === 'asc' ? (
                        <ArrowDropUpIcon />
                      ) : (
                        <ArrowDropDownIcon />
                      )
                    ) : null}
                  </TableCell>
                  <TableCell align='center'>Buy</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {orderBy(
                  query
                    ? marktInventory.filter((x) =>
                        x[columnToQuery].toLowerCase().includes(lowerCaseQuery)
                      )
                    : marktInventory,
                  columnToSort,
                  sortDirection
                )
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <TableRow key={row.cardId}>
                      <TableCell component='th' scope='row'>
                        <p
                          className='card-name'
                          onMouseOut={() => {
                            mouseDeactivate();
                          }}
                          onMouseOver={() => {
                            mouseActive(row.imgUrl, index);
                          }}
                        >
                          {row.name}
                        </p>
                        {imgIndex === index ? (
                          <img className='imgHover' src={imgUrl} />
                        ) : null}
                      </TableCell>
                      <TableCell align='center'>
                        {row.colors.length == 1 ? (
                          <img
                            src={`/img/${row.colors}.png`}
                            className='mana-symbols'
                          ></img>
                        ) : null}
                        {row.colors.length > 1
                          ? row.colors.map((element) => {
                              return (
                                <img
                                  src={`/img/${element}.png`}
                                  className='mana-symbols'
                                  key={element}
                                ></img>
                              );
                            })
                          : null}
                        {row.colors.length < 1 ? (
                          <img
                            src={`/img/none.png`}
                            className='mana-symbols'
                          ></img>
                        ) : null}
                      </TableCell>
                      <TableCell align='center'>{row.rarity}</TableCell>
                      <TableCell align='center'>{row.set}</TableCell>
                      <TableCell align='center'>{row.sellQuantity}</TableCell>
                      <TableCell align='center'>{row.sellPrice}</TableCell>
                      <TableCell align='center'>
                        <Buy sellCardInfo={row} />
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
            <TablePagination
              component='div'
              rowsPerPageOptions={[6, 25, 50]}
              count={marktInventory.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </TableContainer>
        </div>
      ) : null}

      {!ready ? (
        <div className='loader-wrapper'>
          <RingLoader loading size={300} color='#4eb7ee' />
        </div>
      ) : null}
    </div>
  );
}

export default Marketplace;
