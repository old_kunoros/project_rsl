import '../components/css/contact.css';

function Contact() {
  return (
    <div className='bg-img'>
      <div>
        <p className='p1'>Contact</p>
        <h1 className='h1'>Let's get in touch</h1>
        <div className='grid-'>
          <div>
            <img className='svg-img' src='/img/twitter.svg'></img>
            <h1 className='grid-h1'>Twitter</h1>
            <p className='grid-p'>Tweet away</p>
          </div>

          <div>
            <img className='svg-img' src='/img/microphone.svg'></img>
            <h1 className='grid-h1'>Content creator</h1>
            <p className='grid-p'>Tell us about you</p>
          </div>

          <div>
            <img className='svg-img' src='/img/headphones.svg'></img>
            <h1 className='grid-h1'>Support</h1>
            <p className='grid-p'>Help Forum</p>
          </div>

          <div>
            <img className='svg-img' src='/img/envelope.svg'></img>
            <h1 className='grid-h1'>Request</h1>
            <p className='grid-p'>Get in touch</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Contact;
