import axios from 'axios';
import React, { useState, useEffect } from 'react';

import Inventory from '../components/user_inventory';

// Import styling
import '../components/css/user.css';

function User() {
  const [displayName, setDisplayName] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [gold, setGold] = useState(0);

  const [nieuwFirstName, setNieuwFirstName] = useState(firstName);
  const [nieuwLastName, setNieuwLastName] = useState(lastName);

  const [show, setShow] = useState(true);

  useEffect(async () => {
    const response = await axios.get('user/data');
    setDisplayName(response.data.displayName);
    setFirstName(response.data.firstName);
    setLastName(response.data.lastName);
    setEmail(response.data.email);
    setGold(response.data.userGold);
    setNieuwFirstName(response.data.firstName);
    setNieuwLastName(response.data.lastName);
  }, []);

  async function update(e) {
    e.preventDefault();

    try {
      const updateData = {
        nieuwFirstName,
        nieuwLastName,
      };
      await axios.put('auth/profile', updateData);
      window.location.reload(false);
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className='main-wrapper'>
      <div className='toggle-nav'>
        <div
          className='toggle-btn-1'
          onClick={() => {
            if (!show) {
              setShow(true);
            }
          }}
        >
          <h1 className={!show ? 'nav' : 'nav-active'}>account</h1>
        </div>

        <div
          className='toggle-btn-2'
          onClick={() => {
            if (show) {
              setShow(false);
            }
          }}
        >
          <h1 className={show ? 'nav' : 'nav-active'}>inventory</h1>
        </div>
      </div>

      {show ? (
        <div className='account-wrapper'>
          <div className='current-info'>
            <form onSubmit={update}>
              <div className='info-container'>
                <h1 className='h1-black'>
                  display name: <p className='text-color'>{displayName}</p>
                </h1>
              </div>
              <div className='info-container'>
                <h1 className='h1-black'>
                  first name: <p className='text-color'>{firstName}</p>
                </h1>
                <input
                  className='input-account'
                  type='text'
                  placeholder={firstName}
                  onChange={(e) => setNieuwFirstName(e.target.value)}
                  name='firstName'
                />
              </div>
              <div className='info-container'>
                <h1 className='h1-black'>
                  last name: <p className='text-color'>{lastName}</p>
                </h1>
                <input
                  className='input-account'
                  type='text'
                  placeholder={lastName}
                  onChange={(e) => setNieuwLastName(e.target.value)}
                  name='lastName'
                />
              </div>
              <div className='info-container'>
                <h1 className='h1-black'>
                  email name: <p className='text-color'>{email}</p>
                </h1>
              </div>
              <div className='info-container'>
                <h1 className='h1-black'>
                  current gold: <p className='text-color'>{gold}</p>
                </h1>
              </div>
              <input
                className='input-button'
                type='submit'
                placeholder='submit'
              />
            </form>
          </div>
        </div>
      ) : null}

      {!show ? (
        <div className='inventory-wrapper'>
          <Inventory />
        </div>
      ) : null}
    </div>
  );
}

export default User;
