import '../components/css/about.css';

function About() {
  return (
    <div>
      <div className='header-img'>
        <h1 className='absolute h1-header'>Who We Are</h1>
      </div>
      <div className='grid'>
        <h1 className='grid-h1-about'>Etiam sed. placerat libero.</h1>
        <div className='grid-img-1'></div>
        <div className='grid-img-2'></div>
        <p className='grid-p-1'>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
          vestibulum augue non augue ultricies, ut rutrum nibh vestibulum.Lorem
          ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vestibulum
          augue non augue ultricies, ut rutrum nibh vestibulum.
        </p>
        <p className='grid-p-2'>
          Nunc bibendum justo eget efficitur sollicitudin. Suspendisse at elit
          at velit pellentesque auctor. Suspendisse nec augue ante.
        </p>
      </div>
    </div>
  );
}

export default About;
