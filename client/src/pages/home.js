import '../components/css/home.css';

function Home() {
  return (
    <div className='home-wrapper'>
      <h1 className='main-h1'>passages of Lorem Ipsum</h1>

      <div className='div-p'>
        <p className='home-p'>
          Contrary to popular belief, Lorem Ipsum is not simply random text. It
          has roots in a piece of classical Latin literature from 45 BC, making
          it over 2000 years old. Richard McClintock, a Latin professor at
          Hampden-Sydney College in Virginia, looked up one of the more obscure
          Latin words, consectetur, from a Lorem Ipsum passage, and going
          through the cites of the word in classical literature, discovered the
          undoubtable source. Lorem Ipsum
        </p>
      </div>
    </div>
  );
}

export default Home;
