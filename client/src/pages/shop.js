import axios from 'axios';
import React, { useState, useEffect } from 'react';

// Import styling
import '../components/css/shop.css';

// Import UI
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='down' ref={ref} {...props} />;
});

function Shop() {
  const [pack, setPack] = useState([
    { name: 'SOI', img: '/img/SOI_Booster_Pack.png' },
    { name: 'EMN', img: '/img/EMN_Booster_Pack.png' },
    { name: 'KLD', img: '/img/KLD_Booster_Pack.png' },
  ]);
  const [cardData, setCardData] = useState(undefined);
  const [open, setOpen] = useState(false);
  // const [err, setErr] = useState(undefined);

  // Make http request to server.
  async function buyPack(packName) {
    const res = await axios.post('card/pack', {
      packName: packName,
    });
    // Set response to cardData.
    setCardData(res.data);
  }
  // When cardData is upathed open dialog.
  useEffect(() => {
    if (
      cardData != undefined &&
      cardData != 'sorry you dont have enough gold'
    ) {
      handleClickOpen();
    }
  }, [cardData]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const dialogCards = () => {
    if (
      cardData != undefined &&
      cardData != 'sorry you dont have enough gold'
    ) {
      return cardData.map((card) => (
        <div key={card._id} className='card'>
          <img className='card-img' src={card.imgUrl} />
          <h1>{card.name}</h1>
        </div>
      ));
    }

    if (cardData === 'sorry you dont have enough gold') {
      console.log(cardData);
    }
  };

  return (
    <div className='packs-container'>
      {pack.map((pack) => (
        <div key={pack.name} className='pack'>
          <img src={pack.img} />
          <h1>{pack.name}</h1>
          <button
            className='buy-pack-btn'
            onClick={() => {
              buyPack(pack.name);
            }}
          >
            Buy
          </button>
        </div>
      ))}
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
        className='make-transparent'
      >
        <AppBar>
          <Toolbar>
            <IconButton
              edge='start'
              color='inherit'
              onClick={handleClose}
              aria-label='close'
            >
              <span className='material-icons'>close</span>
            </IconButton>
          </Toolbar>
        </AppBar>
        <div className='shop-card-wrapper'>;{dialogCards()}</div>;
      </Dialog>
    </div>
  );
}

export default Shop;
