import React, { useState, useEffect } from 'react';
import axios from 'axios';

// import UI
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const Sell = (props) => {
  const [cardToSell, setCardToSell] = useState(props.sellCardInfo);
  const [open, setOpen] = useState(false);
  const [sellQuantity, setSellQuantity] = useState(cardToSell.quantity);
  const [cardId, setCardId] = useState(cardToSell.cardId);
  const [cashingGold, setCashingGold] = useState(undefined);
  const [tradeValue, setTradeValue] = useState(100);

  const handleClickOpen = () => {
    setOpen(true);
    handleCasingGold();
  };

  const handleClose = () => {
    setOpen(false);
  };

  const MinusSellQuantity = () => {
    if (sellQuantity != 1) {
      setSellQuantity(sellQuantity - 1);
    }
  };

  const PlusSellQuantity = () => {
    if (sellQuantity != cardToSell.quantity) {
      setSellQuantity(sellQuantity + 1);
    }
  };

  const handleCasingGold = () => {
    if (cardToSell.rarity == 'Common') {
      setCashingGold(10);
    } else if (cardToSell.rarity == 'Uncommon') {
      setCashingGold(50);
    } else {
      setCashingGold(80);
    }
  };

  async function cashingCard() {
    const chasingGoldPrize = cashingGold * sellQuantity;
    try {
      const cashingData = {
        sellQuantity,
        chasingGoldPrize,
        cardId,
      };
      await axios.post('user/cashing', cashingData);
    } catch (err) {
      console.log(err);
    }
    window.location.reload(false);
  }

  async function sellToMarked() {
    const marketTradePrize = tradeValue;
    try {
      const marketData = {
        sellQuantity,
        marketTradePrize,
        cardId,
      };
      await axios.post('markt/sell', marketData);
    } catch (err) {
      console.log(err);
    }
    handleClose();
    window.location.reload(false);
  }

  return (
    <div>
      <button className='sell-btn' onClick={handleClickOpen}>
        sell
      </button>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='form-dialog-title'
        maxWidth='xl'
      >
        <div className='sell-wrapper'>
          <div className='quantity-to-sell'>
            <h1 className='sell-h1'>How many do you want to sell?</h1>
            <div className='quantity-wrapper'>
              <button className='quantity-btn' onClick={MinusSellQuantity}>
                <p>-</p>
              </button>
              <div className='quantity-value'>
                <p>{sellQuantity}</p>
              </div>
              <button className='quantity-btn' onClick={PlusSellQuantity}>
                <p>+</p>
              </button>
            </div>
          </div>
          <div className='cashing-sell'>
            <h1 className='sell-h1'>cashing in for {cashingGold} gold</h1>
            <button className='sell-btn extra' onClick={cashingCard}>
              Sell
            </button>
          </div>
          <div className='market-sell'>
            <h1 className='sell-h1'>
              fow how much do you want to put it in on the market
            </h1>
            <div className='market-sell-wrapper'>
              <input
                className='market-sell-input'
                type='number'
                placeholder={cashingGold}
                onChange={(e) => setTradeValue(Number(e.target.value))}
                value={tradeValue}
              />
              <button className='sell-btn' onClick={sellToMarked}>
                Sell
              </button>
            </div>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default Sell;
