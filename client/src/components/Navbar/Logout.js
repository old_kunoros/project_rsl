import axios from 'axios';
import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';

// Import components
import AuthContext from '../../context/AuthContext';

export default function Logout() {
  // Get the getLoggedIn fuction from AuthContext component
  const { getLoggedIn } = useContext(AuthContext);
  const history = useHistory();

  async function logout() {
    await axios.get('auth/logout');
    await getLoggedIn();
    history.push('/');
  }
  return (
    <button className='NavBtnLink' onClick={logout}>
      Logout
    </button>
  );
}
