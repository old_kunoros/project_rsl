import React from 'react';
import { NavLink } from 'react-router-dom';

// Import components
import LoginForm from '../Navbar/LoginForm';
import RegisterForm from '../Navbar/RegisterForm';
import Logout from '../Navbar/Logout';
import AuthContext from '../../context/AuthContext';

// Import styling
import '../css/navbar.css';

function Navbar() {
  // Gets the loggedIn boolen to see what components to load
  const { loggedIn } = React.useContext(AuthContext);
  // async function add50Gold() {
  //   try {
  //     const response = await axios.get('http://localhost:8080/user/addGold');
  //     console.log(response.data);
  //   } catch (err) {
  //     console.log(err);
  //   }
  // }

  return (
    <div className='Nav'>
      <NavLink className='NavLink' to='/'>
        <h1>Home</h1>
      </NavLink>
      <div className='Bars' />
      <div className='NavMenu'>
        <NavLink className='NavLink' to='/shop'>
          Shop
        </NavLink>
        <NavLink className='NavLink' to='/marketplace'>
          Marketplace
        </NavLink>
        <NavLink className='NavLink' to='/about'>
          About
        </NavLink>
        <NavLink className='NavLink' to='/contact'>
          Contact
        </NavLink>
      </div>
      {loggedIn === false && (
        <>
          <div className='NavBtn'>
            <LoginForm />
            <RegisterForm />
          </div>
        </>
      )}
      {loggedIn === true && (
        <div className='NavBtn'>
          <Logout />
        </div>
      )}
      {/* {loggedIn === true && <div className='NavBtn'></div>} */}
      {loggedIn === true && (
        <div className='NavBtn'>
          <NavLink className='NavLink' to='/user'>
            user Profile
          </NavLink>
        </div>
      )}
    </div>
  );
}

export default Navbar;
