// Import packages
import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

// Import components
import AuthContext from '../../context/AuthContext';

// Import styling
import '../css/loginForm.css';

// Import UI
import Dialog from '@material-ui/core/Dialog';

function LoginForm() {
  const [open, setOpen] = useState(false);
  const [displayName, setDisplayName] = useState('');
  const [password, setPasword] = useState('');
  // Gets the loggedIn boolen to see what components to load
  const { getLoggedIn } = useContext(AuthContext);
  const history = useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  async function login(e) {
    e.preventDefault();

    try {
      const loginData = {
        displayName,
        password,
      };
      await axios.post('auth/login', loginData);
      await getLoggedIn();
      history.push('/');
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div>
      <button className='NavBtnLink' onClick={handleClickOpen}>
        Sign In
      </button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='form-dialog-title'
      >
        <div className='login-wrapper'>
          <h1 className='h1-login'>Log in to your account</h1>
          <form onSubmit={login}>
            <div className='input-container'>
              <input
                type='text'
                placeholder='Displayname'
                onChange={(e) => setDisplayName(e.target.value)}
                value={displayName}
                className='input-1 input'
              />
              <input
                type='password'
                placeholder='Password'
                onChange={(e) => setPasword(e.target.value)}
                value={password}
                className='input-2 input'
              />
            </div>

            <button className='btn' type='submit'>
              Log in
            </button>
          </form>
        </div>
      </Dialog>
    </div>
  );
}

export default LoginForm;
