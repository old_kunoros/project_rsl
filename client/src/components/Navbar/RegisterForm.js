// Import packages
import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

// Import components
import AuthContext from '../../context/AuthContext';

// Import styling
import '../css/logoutForm.css';

// Import UI
import Dialog from '@material-ui/core/Dialog';

function RegisterForm() {
  const [open, setOpen] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPasword] = useState('');
  const [passwordVerify, setPasswordVerify] = useState('');
  const [displayName, setDisplayName] = useState('');
  // Gets the loggedIn boolen to see what components to load
  const { getLoggedIn } = useContext(AuthContext);
  const history = useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  async function register(e) {
    e.preventDefault();

    try {
      const registerData = {
        firstName,
        lastName,
        email,
        password,
        passwordVerify,
        displayName,
      };
      await axios.post('auth/register', registerData);
      await getLoggedIn();
      history.push('/');
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div>
      <button className='NavBtnLink' onClick={handleClickOpen}>
        Sign Up
      </button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='form-dialog-title'
      >
        <div className='register-wrapper'>
          <h1 className='h1-register'>Register a new account</h1>
          <form onSubmit={register}>
            <div className='input-wrapper'>
              <input
                type='text'
                placeholder='First name'
                onChange={(e) => setFirstName(e.target.value)}
                value={firstName}
              />
              <input
                type='text'
                placeholder='Last name'
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
              />
              <input
                type='emial'
                placeholder='Email'
                onChange={(e) => setEmail(e.target.value)}
                value={email}
              />
              <input
                type='password'
                placeholder='Password'
                onChange={(e) => setPasword(e.target.value)}
                value={password}
              />
              <input
                type='password'
                placeholder='Password'
                onChange={(e) => setPasswordVerify(e.target.value)}
                value={passwordVerify}
              />
              <input
                type='text'
                placeholder='Displayname'
                onChange={(e) => setDisplayName(e.target.value)}
                value={displayName}
              />
            </div>

            <button className='btn' type='submit'>
              Register
            </button>
          </form>
        </div>
      </Dialog>
    </div>
  );
}

export default RegisterForm;
