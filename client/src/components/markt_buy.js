import React, { useState, useEffect } from 'react';
import axios from 'axios';

// import UI
import Dialog from '@material-ui/core/Dialog';

const Buy = (props) => {
  const [cardToBuy, setCardToSell] = useState(props.sellCardInfo);
  const [open, setOpen] = useState(false);
  const [cashingGold, setCashingGold] = useState(undefined);
  const [tradeValue, setTradeValue] = useState(100);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  async function buyCard() {
    try {
      const marketBuyData = {
        cardOwnerId: cardToBuy.user_id,
        sellPrice: cardToBuy.sellPrice,
        sellQuantity: cardToBuy.sellQuantity,
        cardId: cardToBuy.cardId,
      };
      window.location.reload(false);
      await axios.post('markt/buy', marketBuyData);
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div>
      <button className='sell-btn' onClick={handleClickOpen}>
        Buy
      </button>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='form-dialog-title'
      >
        <div className='sure-wrapper'>
          <h1 className='sure-h1'>Are you sure you want to buy this card?</h1>
          <div className='sure-btns'>
            <button className='sure-btn' onClick={handleClose}>
              No
            </button>
            <button className='sure-btn' onClick={buyCard}>
              Yes
            </button>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default Buy;
