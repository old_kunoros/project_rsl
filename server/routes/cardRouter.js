const express = require('express');
const Card = require('../models/cardModel');
const User = require('../models/userModel');
const auth = require('../middleware/auth');
const mtg = require('mtgsdk');
const jwt = require('jsonwebtoken');

const router = express.Router();

function getRandomCard(min, max) {
  let step1 = max - min + 1;
  let step2 = Math.random() * step1;
  let result = Math.floor(step2) + min;

  return result;
}

router.post('/pack', auth, async (req, res) => {
  try {
    const token = req.cookies.token;
    const userId = jwt.decode(token);
    const { packName } = req.body;
    const allCommon = await Card.find({ set: packName, rarity: 'Common' });
    const allUncommon = await Card.find({ set: packName, rarity: 'Uncommon' });
    const allRare = await Card.find({ set: packName, rarity: 'Rare' });
    const pack = [];
    const userGold = await User.find(
      { _id: userId.user },
      { userGold: 1, _id: 0 }
    );
    const packCost = 100;

    if (userGold[0].userGold < packCost) {
      res.send('sorry you dont have enough gold');
    } else {
      // Set nieuw user gold value
      const updatedGold = userGold[0].userGold - packCost;
      await User.findOneAndUpdate(
        { _id: userId.user },
        {
          $set: { userGold: updatedGold },
        }
      );

      // Get 5 random common card and insert it into pack
      for (let i = 0; i < 5; i++) {
        let Index = getRandomCard(0, allCommon.length - 1);
        pack.push(allCommon[Index]);
      }
      // Get 2 random uncommon card and insert it into pack
      for (let i = 0; i < 2; i++) {
        let Index = getRandomCard(0, allUncommon.length - 1);
        pack.push(allUncommon[Index]);
      }
      // Get 1 random rare card and insert it into pack
      for (let i = 0; i < 1; i++) {
        let Index = getRandomCard(0, allRare.length - 1);
        pack.push(allRare[Index]);
      }

      // For all cards in pack see if exist or add 1 to quantity
      for (let i = 0; i < pack.length; i++) {
        const existingCard = await User.findOne({
          _id: userId.user,
          'inventory.cardId': pack[i]._id,
        });

        if (!existingCard) {
          // ! if result is false card is not in the inventory // add to inventory
          await User.findOneAndUpdate(
            { _id: userId.user },
            {
              $push: {
                inventory: { cardId: pack[i]._id, quantity: 1 },
              },
            }
          );
        } else {
          // ! if result is true card is in inventory // update quantity by one
          const cardIdUpdate = String(pack[i]._id);
          // curent inventory of the user
          const [curentInventory] = await User.find(
            {
              _id: userId.user,
              'inventory.cardId': pack[i]._id,
            },
            { _id: 0, 'inventory.cardId': 1, 'inventory.quantity': 1 }
          );
          // update the quantity of the card
          for (let j = 0; j < curentInventory.inventory.length; j++) {
            if (curentInventory.inventory[j].cardId == cardIdUpdate) {
              let addQuantityOfCard = curentInventory.inventory[j].quantity + 1;
              let oldQuantityOfCard = addQuantityOfCard - 1;
              // add  card with one more quantity whan the original
              await User.findOneAndUpdate(
                {
                  _id: userId.user,
                  'inventory.cardId': pack[i]._id,
                },
                {
                  $addToSet: {
                    inventory: {
                      cardId: pack[i]._id,
                      quantity: addQuantityOfCard,
                    },
                  },
                }
              );
              // delete the original card
              await User.findOneAndUpdate(
                {
                  _id: userId.user,
                  'inventory.cardId': pack[i]._id,
                  'inventory.quantity': oldQuantityOfCard,
                },
                {
                  $pull: {
                    inventory: {
                      cardId: pack[i]._id,
                      quantity: oldQuantityOfCard,
                    },
                  },
                }
              );
            }
          }
        }
      }
      res.json(pack);
    }
  } catch (err) {
    console.error(err);
    res.status(500).send();
  }
});

// router.post('/add', auth, async (req, res) => {
//   try {
//     const { name, rarity, set, imgUrl, id } = req.body;

//     const newCard = new Card({
//       name,
//       rarity,
//       set,
//       imgUrl,
//       id,
//     });

//     const savedCard = await newCard.save();

//     res.json(savedCard);
//   } catch (err) {
//     console.error(err);
//     res.status(500).send();
//   }
// });

// async function insertCards(Set) {
//   mtg.card
//     .all({
//       set: `${Set}`,
//     })
//     .on('data', (card) => {
//       if (card.supertypes != 'Basic') {
//         let cardName = card.name;
//         let cardColors = card.colors;
//         let cardTypes = card.types;
//         let cardRarity = card.rarity;
//         let cardSet = card.set;
//         let cardLayout = card.layout;
//         let cardImageUrl = card.imageUrl;
//         const newCard = new Card({
//           name: cardName,
//           colors: cardColors,
//           types: cardTypes,
//           rarity: cardRarity,
//           set: cardSet,
//           layout: cardLayout,
//           imgUrl: cardImageUrl,
//         });
//         newCard.save();
//       }
//     });
// }
// insertCards('KLD');
module.exports = router;
