const express = require('express');
const User = require('../models/userModel');
const Card = require('../models/cardModel');
// const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const auth = require('../middleware/auth');
const { card } = require('mtgsdk');

const router = express.Router();

router.get('/data', auth, (req, res) => {
  try {
    const token = req.cookies.token;
    const userId = jwt.decode(token);

    User.findOne({ _id: userId.user }, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        res.json(data);
      }
    });

    if (!token) {
      return res.json(false);
    }
  } catch (err) {
    console.error(err);
    res.json(false);
  }
});

router.get('/inventory', auth, async (req, res) => {
  try {
    const token = req.cookies.token;
    const userId = jwt.decode(token);
    const user_inventory = [];
    const cards = [];
    let index = 0;

    // Get user inventory
    const [getUserInventory] = await User.find(
      { _id: userId.user },
      { inventory: 1, _id: 0 }
    );
    // Get the cardId and quantity from the user inventory push it to the uer_inventory array
    for (let i = 0; i < getUserInventory.inventory.length; i++) {
      const element = getUserInventory.inventory[i];
      user_inventory.push(element);
    }
    // Get card details useing cardId from user_inventory
    for (let i = 0; i < user_inventory.length; i++) {
      const element = user_inventory[i];
      const [card] = await Card.find({ _id: element.cardId }, { _id: 0 });
      cards.push(card);
    }
    // Add the card details to the user_inventory
    user_inventory.forEach((a) => {
      a.colors = cards[index].colors;
      a.types = cards[index].types;
      a.name = cards[index].name;
      a.rarity = cards[index].rarity;
      a.set = cards[index].set;
      a.layout = cards[index].layout;
      a.imgUrl = cards[index].imgUrl;
      index++;
    });

    res.json(user_inventory);
  } catch (err) {
    console.log(err);
  }
});

router.post('/cashing', auth, async (req, res) => {
  try {
    const token = req.cookies.token;
    const userId = jwt.decode(token);
    const cardId = String(req.body.cardId);
    const sellQuantity = req.body.sellQuantity;
    const chasingGoldPrize = req.body.chasingGoldPrize;

    const [userGold] = await User.find(
      { _id: userId.user },
      { userGold: 1, _id: 0 }
    );
    const [getUsercards] = await User.find(
      {
        _id: userId.user,
      },
      { _id: 0, 'inventory.cardId': 1, 'inventory.quantity': 1 }
    );

    for (let i = 0; i < getUsercards.inventory.length; i++) {
      if (getUsercards.inventory[i].cardId == cardId) {
        const cardQuantity = getUsercards.inventory[i].quantity;
        const cardIdUse = getUsercards.inventory[i].cardId;
        if (sellQuantity < cardQuantity) {
          console.log(`je hebt nog ${cardQuantity - sellQuantity}.`);
          // add updated card
          await User.findOneAndUpdate(
            {
              _id: userId.user,
            },
            {
              $addToSet: {
                inventory: {
                  cardId: cardIdUse,
                  quantity: cardQuantity - sellQuantity,
                },
              },
            }
          );
          // delete the original card
          await User.findOneAndUpdate(
            {
              _id: userId.user,
              'inventory.cardId': cardIdUse,
              'inventory.quantity': cardQuantity,
            },
            {
              $pull: {
                inventory: {
                  cardId: cardIdUse,
                  quantity: cardQuantity,
                },
              },
            }
          );
        } else {
          console.log('You dont have any more of this card.');
          await User.findOneAndUpdate(
            {
              _id: userId.user,
              'inventory.cardId': cardIdUse,
              'inventory.quantity': cardQuantity,
            },
            {
              $pull: {
                inventory: {
                  cardId: cardIdUse,
                  quantity: cardQuantity,
                },
              },
            }
          );
        }
      }
      const newGoldValue = userGold.userGold + chasingGoldPrize;
      await User.findOneAndUpdate(
        { _id: userId.user, userGold: userGold.userGold },
        { $set: { userGold: newGoldValue } }
      );
    }

    res.send('done!!');
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
