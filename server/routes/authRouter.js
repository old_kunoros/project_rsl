const express = require('express');
const User = require('../models/userModel');
const Markt = require('../models/marktModel');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const router = express.Router();

// route

// Register route
router.post('/register', async (req, res) => {
  try {
    const {
      firstName,
      lastName,
      email,
      password,
      passwordVerify,
      displayName,
    } = req.body;

    // !Validation
    // looks if all fields are filld out
    if (
      !firstName ||
      !lastName ||
      !email ||
      !password ||
      !passwordVerify ||
      !displayName
    ) {
      return res
        .status(400)
        .json({ errorMessage: 'Please enter all required fields.' });
    }
    // Looks if the password length is at least 6 characters long
    if (password.length < 6) {
      return res.status(400).json({
        errorMessage: 'Please enter a password of at least 6 characters.',
      });
    }
    // Looks if the passwords match
    if (password !== passwordVerify) {
      return res.status(400).json({
        errorMessage: 'Please enter the same password twice.',
      });
    }
    // Looks if there is a existing user
    const existingUser = await User.findOne({ email: email });
    if (existingUser) {
      return res.status(400).json({
        errorMessage: 'An account with this email already exists.',
      });
    }
    // Hash the password
    const salt = await bcrypt.genSalt();
    const passwordHash = await bcrypt.hash(password, salt);

    // Save a new user account to the db
    const newUser = new User({
      firstName,
      lastName,
      email,
      passwordHash,
      displayName,
    });
    const savedUser = await newUser.save();

    // Make market collection for the user
    const user_id = savedUser._id;
    const newMarkt = new Markt({
      user_id,
    });
    newMarkt.save();

    // sign the token
    const token = jwt.sign(
      {
        user: savedUser._id,
      },
      process.env.JWT_SECRET
    );

    // send the token in HTTP-only cookie
    res
      .cookie('token', token, {
        httpOnly: true,
      })
      .send();
  } catch (err) {
    console.log(err);
    res.status(500).send;
  }
});

// Login route
router.post('/login', async (req, res) => {
  try {
    const { displayName, password } = req.body;

    // !Validation
    // Looks if all fields are filld out
    if (!displayName || !password) {
      return res
        .status(400)
        .json({ errorMessage: 'Please enter all required fields.' });
    }

    // Looks if password or display name exist
    const existingUser = await User.findOne({ displayName });
    if (!existingUser) {
      return res.status(401).json({ errorMessage: 'Wrong email or password.' });
    }
    // Looks if passwords match
    const passwordCorrect = await bcrypt.compare(
      password,
      existingUser.passwordHash
    );
    if (!passwordCorrect) {
      return res
        .status(400)
        .json({ errorMessage: 'Please enter all required fields.' });
    }

    // sign the token
    const token = jwt.sign(
      {
        user: existingUser._id,
      },
      process.env.JWT_SECRET
    );

    // send the token in HTTP-only cookie
    res
      .cookie('token', token, {
        httpOnly: true,
      })
      .send();
  } catch (err) {
    console.error(err);
    res.status(500).send();
  }
});

// Logout route
router.get('/logout', (req, res) => {
  res
    .cookie('token', '', {
      httpOnly: true,
      expires: new Date(0),
    })
    .send();
});

// Logged in route
router.get('/loggedIn', (req, res) => {
  try {
    const token = req.cookies.token;

    if (!token) {
      return res.json(false);
    }
    res.send(true);
  } catch (err) {
    console.error(err);
    res.json(false);
  }
});

// update user profile
router.put('/profile', async (req, res) => {
  try {
    const token = req.cookies.token;
    const userId = jwt.decode(token);
    const { nieuwFirstName, nieuwLastName } = req.body;

    await User.findOneAndUpdate(
      {
        _id: userId.user,
      },
      {
        $set: { firstName: nieuwFirstName, lastName: nieuwLastName },
      }
    );

    res.send('account information has been changed.');
  } catch (err) {
    console.error(err);
  }
});

module.exports = router;
