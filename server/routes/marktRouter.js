const express = require('express');
const User = require('../models/userModel');
const Card = require('../models/cardModel');
const Markt = require('../models/marktModel');
const jwt = require('jsonwebtoken');
const auth = require('../middleware/auth');
// const { card } = require('mtgsdk');

const router = express.Router();

router.post('/sell', auth, async (req, res) => {
  try {
    const token = req.cookies.token;
    const userId = jwt.decode(token);
    const cardId = String(req.body.cardId);
    const sellQuantity = req.body.sellQuantity;
    const marketTradePrize = req.body.marketTradePrize;

    const [getUsercards] = await User.find(
      {
        _id: userId.user,
      },
      { _id: 0, 'inventory.cardId': 1, 'inventory.quantity': 1 }
    );

    for (let i = 0; i < getUsercards.inventory.length; i++) {
      if (getUsercards.inventory[i].cardId == cardId) {
        const cardQuantity = getUsercards.inventory[i].quantity;
        const cardIdUse = getUsercards.inventory[i].cardId;
        if (sellQuantity < cardQuantity) {
          // add card to market inventory
          await Markt.findOneAndUpdate(
            {
              user_id: userId.user,
            },
            {
              $addToSet: {
                marktInventory: {
                  cardId: cardIdUse,
                  sellQuantity: sellQuantity,
                  sellPrice: marketTradePrize,
                },
              },
            }
          );
          // add updated card value to user inventory
          await User.findOneAndUpdate(
            {
              _id: userId.user,
            },
            {
              $addToSet: {
                inventory: {
                  cardId: cardIdUse,
                  quantity: cardQuantity - sellQuantity,
                },
              },
            }
          );
          // delete the card from user inventory
          await User.findOneAndUpdate(
            {
              _id: userId.user,
              'inventory.cardId': cardIdUse,
              'inventory.quantity': cardQuantity,
            },
            {
              $pull: {
                inventory: {
                  cardId: cardIdUse,
                  quantity: cardQuantity,
                },
              },
            }
          );
        } else {
          // add card to market inventory
          await Markt.findOneAndUpdate(
            {
              user_id: userId.user,
            },
            {
              $addToSet: {
                marktInventory: {
                  cardId: cardIdUse,
                  sellQuantity: sellQuantity,
                  sellPrice: marketTradePrize,
                },
              },
            }
          );
          await User.findOneAndUpdate(
            {
              _id: userId.user,
              'inventory.cardId': cardIdUse,
              'inventory.quantity': cardQuantity,
            },
            {
              $pull: {
                inventory: {
                  cardId: cardIdUse,
                  quantity: cardQuantity,
                },
              },
            }
          );
        }
      }
    }
    res.send('done!!');
  } catch (err) {
    console.log(err);
  }
});

router.get('/inventory', async (req, res) => {
  try {
    const token = req.cookies.token;
    const userId = jwt.decode(token);
    let index = 0;
    const allMarktInventory = await Markt.find(
      {},
      { marktInventory: 1, user_id: 1 }
    );
    const completeMarkt = [];

    const cards = [];

    // Get all cards from markt collection and add user Id to it
    for (let i = 0; i < allMarktInventory.length; i++) {
      const inventory = allMarktInventory[i];
      const user_Id = inventory.user_id;

      inventory.marktInventory.map((e) => {
        e.user_id = user_Id;
        completeMarkt.push(e);
      });
    }

    // Get card details useing cardId from user_inventory
    for (let i = 0; i < completeMarkt.length; i++) {
      const element = completeMarkt[i];
      const [card] = await Card.find({ _id: element.cardId }, { _id: 0 });
      cards.push(card);
    }

    // Add the card details to the user_inventory
    completeMarkt.forEach((a) => {
      a.colors = cards[index].colors;
      a.types = cards[index].types;
      a.name = cards[index].name;
      a.rarity = cards[index].rarity;
      a.set = cards[index].set;
      a.layout = cards[index].layout;
      a.imgUrl = cards[index].imgUrl;
      index++;
    });

    res.json(completeMarkt);
  } catch (err) {
    console.log(err);
  }
});

router.post('/buy', auth, async (req, res) => {
  try {
    const token = req.cookies.token;
    const userId = jwt.decode(token);
    const cardOwnerId = String(req.body.cardOwnerId);
    const sellPrice = req.body.sellPrice;
    const sellQuantity = req.body.sellQuantity;
    const cardId = String(req.body.cardId);

    const [userGold] = await User.find(
      { _id: userId.user },
      { userGold: 1, _id: 0 }
    );

    const [cardOwnerGold] = await User.find(
      { _id: cardOwnerId },
      { userGold: 1, _id: 0 }
    );
    const newUserGoldValue = userGold.userGold - sellPrice;
    const newCardOwnerGoldValue = cardOwnerGold.userGold + sellPrice;

    if (userGold.userGold < sellPrice) {
      console.log('you cant buy this card');
      return req.send('you cant buy this card');
    } else {
      // Get the onwers market en find the card to remove
      const ownersMarket = await Markt.findOne({
        user_id: cardOwnerId,
      });

      for (let i = 0; i < ownersMarket.marktInventory.length; i++) {
        const element = ownersMarket.marktInventory[i];
        if (
          element.cardId == cardId &&
          element.sellQuantity == sellQuantity &&
          element.sellPrice == sellPrice
        ) {
          //! delete the card from the owners market invenotroy
          await Markt.findOneAndUpdate(
            {
              'marktInventory.cardId': element.cardId,
              'marktInventory.sellQuantity': element.sellQuantity,
              'marktInventory.sellPrice': element.sellPrice,
            },
            {
              $pull: {
                marktInventory: {
                  cardId: element.cardId,
                  sellQuantity: element.sellQuantity,
                  sellPrice: element.sellPrice,
                },
              },
            }
          );

          const userInventory = await User.findOne(
            {
              _id: userId.user,
            },
            { _id: 0, 'inventory.cardId': 1, 'inventory.quantity': 1 }
          );
          //! Add the card to users inventory if one coppy of the card exists
          for (let j = 0; j < userInventory.inventory.length; j++) {
            const element = userInventory.inventory[j];
            const cardIdUse = userInventory.inventory[i].cardId;
            if (element.cardId == cardId) {
              // add the new card to users invenotry
              await User.findOneAndUpdate(
                {
                  _id: userId.user,
                },
                {
                  $addToSet: {
                    inventory: {
                      cardId: cardId,
                      quantity: element.quantity + sellQuantity,
                    },
                  },
                }
              );
              // delete the card with the old data
              await User.findOneAndUpdate(
                {
                  _id: userId.user,
                },
                {
                  $pull: {
                    inventory: {
                      cardId: cardIdUse,
                      quantity: element.quantity,
                    },
                  },
                }
              );

              // set new gold value for user
              await User.findOneAndUpdate(
                { _id: userId.user },
                { $set: { userGold: newUserGoldValue } }
              );
              // set new gold value for card owner
              await User.findOneAndUpdate(
                { _id: cardOwnerId },
                { $set: { userGold: newCardOwnerGoldValue } }
              );
              return;
            }
          }

          // Add the card to users inventory
          await User.findOneAndUpdate(
            {
              _id: userId.user,
            },
            {
              $addToSet: {
                inventory: {
                  cardId: cardId,
                  quantity: sellQuantity,
                },
              },
            }
          );

          // set new gold value for user
          await User.findOneAndUpdate(
            { _id: userId.user },
            { $set: { userGold: newUserGoldValue } }
          );
          // set new gold value for card owner
          await User.findOneAndUpdate(
            { _id: cardOwnerId },
            { $set: { userGold: newCardOwnerGoldValue } }
          );
        }
      }
    }
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
