const mongoose = require('mongoose');

// Schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true },
  passwordHash: { type: String, required: true },
  displayName: { type: String, required: true },
  userGold: { type: Number, default: 500 },
  inventory: { type: Array, default: [], required: true },
});

// Model
const User = mongoose.model('User', UserSchema);

module.exports = User;
