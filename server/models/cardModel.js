const mongoose = require('mongoose');

const cardSchema = new mongoose.Schema({
  name: { type: String, require: true },
  colors: { type: Array, require: false },
  types: { type: Array, require: true },
  rarity: { type: String, require: true },
  set: { type: String, require: true },
  layout: { type: String, require: true },
  imgUrl: { type: String, require: true },
});

const Card = mongoose.model('card', cardSchema);

module.exports = Card;
