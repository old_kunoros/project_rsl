const mongoose = require('mongoose');

// Schema
const Schema = mongoose.Schema;
const MarktSchema = new Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
  marktInventory: { type: Array, default: [], required: true },
});

// Model
const Markt = mongoose.model('Markt', MarktSchema);

module.exports = Markt;
